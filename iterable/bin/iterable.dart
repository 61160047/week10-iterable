import 'package:iterable/iterable.dart' as iterable;

void main(List<String> arguments) {
  Iterable<int> iterable = [1, 2, 3];
  int value = iterable.elementAt(1);
  print(value);

  //Set, list -> Iterable
  const foods = ['Salad', 'Popcorn', 'Toast', 'Lasagne', 'Mamama'];
  Iterable<String> iterablefoods = foods;
  for (var food in foods) {
    print(food);
  }

  print('First element is ${foods.first}');
  print('Last element is ${iterablefoods.last}');

  var founditem1 = foods.firstWhere((element) {
    return element.length > 5;
    });
  print(founditem1);

  var founditem2 = foods.firstWhere((element) => element.length > 5);
  print(founditem2);

  bool predicate(String item) {
    return item.length > 5;
  }
  var founditem3 = foods.firstWhere(predicate);
  print(founditem3);

  var founditem4 = foods.firstWhere((item) => item.length > 10,
  orElse:() => 'None');
  print(founditem4);

  var founditem5 = foods.firstWhere((item) => item.startsWith('M') && item.contains('a'),
  orElse:() => 'None');
  print(founditem5);

  if(foods.any((item) => item.contains('a'))) {
    print('At least on item contains "a"');
  }

  if (foods.every((item) => item.length >= 5)) {
    print('All item have length >= 5');
  }

  var numbers = const [1, -2, 3, 42, 0, 4, 5, 6];
  var evenNumbers = numbers.where((number) => number.isEven);
  for (var number in evenNumbers) {
    print('$number is even.');
  }

  if(evenNumbers.any((number) => number.isNegative)) {
    print('EvenNumbers contains negative numbers.');
  }

  var largeNumber = evenNumbers.where((number) => number > 1000);
  if(largeNumber.isEmpty) {
    print('largeNumbers is empty!');
  }

  var numberUnitZero = numbers.takeWhile((number) => number !=0);
  print('Numbers unit 0: $numberUnitZero');

  var numberStartingZero = numbers.skipWhile((number) => number != 0);
  print('Numbers starting at 0: $numberStartingZero');

  var numbersBytwo = const [1, -2, 3, 42].map((number) => number*2);
  print('Numbers: $numbersBytwo');
}
